const assert = require("chai").assert
const controller = require('../../controllers/cleaningItemController')
let cleaningItem;



describe("Test af cleaningItemController", function () {
    before(async function () {
        cleaningItem = await controller.createcleaningItem('Dummy', 'Rens ovn')
    });

    it('Testing createCleaningItem', function(){
        assert.ok(cleaningItem)
    })

    it('Testing name Dummy of cleaningItem', function () {
        assert.equal(cleaningItem.name, 'Dummy');
    })

    it('Testing description rens ovn of cleaningItem', function () {
        assert.equal(cleaningItem.description, 'Rens ovn');
    })

    it('Testing getCleaningItem', async function () {
        let getcleaningItem = await controller.getCleaningItem('Dummy')
        assert.equal(getcleaningItem.name, cleaningItem.name)
    })

    it('Testing getCleaningItems', async function () {
        let getcleaningItems = await controller.getCleaningItems()
        assert.ok(getcleaningItems)
    })

    it('testing deleteCleaningItem', async function () {
        const testElement = await controller.createcleaningItem('2TestElement', 'hej');
        await controller.deleteCleaningItem(testElement.name)
        const dbElement = await controller.getCleaningItem(testElement.name)
        assert.isNull(dbElement)
    })

    after(async function () {
        await controller.deleteCleaningItem(cleaningItem.name)
    })
})
