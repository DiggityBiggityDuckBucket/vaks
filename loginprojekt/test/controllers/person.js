let assert = require('assert');
const config = require('../../config.js')
const controller = require('../../controllers/personController')
let person;


describe('Test af person controller', function() {
    before(async function(){
        this.timeout(10000)
        person = await controller.createPerson('Dummy', '12345678')
    })

    it('Testing name of person', function() {
     assert.equal(person.name, 'Dummy');
    });

    it('Testing phone', function(){
      assert.equal(person.phone, '12345678')
    })

    it('testing createPerson og getPerson', async function(){
     const testperson = await controller.createPerson('Testperson','1234');
    
     const dbperson = await controller.getPerson(testperson._id)

     assert.equal(testperson._id.toString(), dbperson._id.toString())
     assert.equal(testperson.name, dbperson.name)
     assert.equal(testperson.phone, dbperson.phone)
     assert.equal(testperson.password, dbperson.password)
     controller.deletePerson(testperson._id)
    })

    it('testing deletePerson', async function(){
      const testperson = await controller.createPerson('Testperson','1234');
      await controller.deletePerson(testperson._id)
      const dbperson = await controller.getPerson(testperson._id)
      assert.equal(null, dbperson)
    })

    it('Testing setKodeord', async function(){
      //
      await controller.setPassword(person._id,'123456')
      person = await controller.getPerson(person._id) 
      assert.equal(person.password, '123456')
    })

  after(async function(){
    await controller.deletePerson(person._id)
  })
});