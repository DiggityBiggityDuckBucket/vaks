const assert = require("chai").assert

const weekController = require('../../controllers/weekController')

let week;


describe("Testing weekController", function() {
    before(async function() {
        week = await weekController.createWeek(1, 1970)
    });

    it('Testing number of uge', function() {
        assert.equal(week.weekNumber, 1);
    })
    it('Testing arr of uge ', function() {
        assert.equal(week.year, 1970);
    })

    it('testing getUger, amount should be higher than 0', async function() {
        const weekCount = await weekController.getWeeks()
        assert.isAbove(weekCount.length, 0)
    })

    it('testing getUger is array', async function() {
        const weekCount = await weekController.getWeeks()
        assert.isArray(weekCount, true)
        
    })


    after(async function() {
      
        
    })
})