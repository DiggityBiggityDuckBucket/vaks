const assert = require("chai").assert
const controller = require('../../controllers/cleaningRecordController')
const weekController = require('../../controllers/weekController')
const itemController = require('../../controllers/cleaningItemController')
let cleaningRecord;
let week;
let cleaningItem;


describe("cleaningRecord", function () {
  before(async function () {
    cleaningItem = await itemController.createcleaningItem('Dummy', 'Rens ovn')
    week = await weekController.createWeek(9, 1970)
    cleaningRecord = await controller.createCleaningRecord(week.id, cleaningItem.id)
    
  });

  it('Testing create of Cleaningrecord', function () {
    const one = cleaningRecord.week._id.toString();
    const two = week._id.toString()
    assert.equal(one, two );

    const item1 = cleaningRecord.cleaningItem._id.toString()
    const item2 = cleaningItem._id.toString()
    assert.equal(item1, item2)
  })
 
  it('Testing getCleaningRecordsForWeek', async function () {
    const dbcleaningRecords = await controller.getCleaningRecordsForWeek(week._id)
    
    assert.isArray(dbcleaningRecords, true);
    assert.isAbove(dbcleaningRecords.length, 0);
  })
  
  after(async function () {
    await weekController.deleteWeek(week.weekNumber, week.year)
    await itemController.deleteCleaningItem(cleaningItem.name)
    controller.deleteCleaningRecord(week.id)
    
  })
});
