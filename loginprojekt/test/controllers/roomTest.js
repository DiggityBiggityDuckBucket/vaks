const assert = require("chai").assert
const config = require('../../config.js')
const controller = require('../../controllers/roomController')
const personController = require('../../controllers/personController')

let room;
let person;

describe('Test af room controller', function () {
    before(async function () {
        room = await controller.createRoom(18, 18);
        person = await personController.createPerson("TestPersonRoom", 98765432)

    })

    it('Testing getRoom', async function () {
        let getRoom = await controller.getRoom(18, 18);
        let getRoomID = "" + getRoom._id
        let roomID = "" + room._id
        let notEqualgetRoom = await controller.getRoom(1, 1);
        assert.equal(getRoomID, roomID);
        assert.notEqual(room, notEqualgetRoom);
    })

    it('Testing getRoomByID', async function () {
        let getRoom = await controller.getRoomByID(room._id);
        let getRoomID = "" + getRoom._id
        let roomID = "" + room._id
        assert.equal(getRoomID, roomID);
    })

    it('Testing getRooms', async function () {
        let rooms = await controller.getRooms()
        assert.isArray(rooms)
        assert.isAbove(rooms.length, 0)
    }
    );

    it('Testing getHalls', async function () {
        let halls = await controller.getHalls()
        assert.isArray(halls)
        assert.isAbove(halls.length, 0)
    }
    );

    it('Testing roomsByHall', async function () {
        let rooms = await controller.getRoomsByHall(18)
        assert.isArray(rooms)
        assert.isAbove(rooms.length, 0)
    }
    );


    it('Testing addPersonToRoom', async function () {
        await controller.addPersonToRoom(room._id, person._id)
        let newRoom = await controller.getRoomByID(room._id)

        assert.equal("" + newRoom.person._id, "" + person._id)
    })

    it('Testing deleteRoom', async function () {
        let deleteRoom = await controller.createRoom(20, 20)
        await controller.deleteRoom(20, 20)
        let getRoom = await controller.getRoomByID(deleteRoom._id)
        assert.equal(null, getRoom)
    })

    it('Testing deleteRooms (many)', async function () {
        let deleteRoom = await controller.createRoom(20, 20)
        let deleteRoom2 = await controller.createRoom(20, 20)
        let deleteRoom3 = await controller.createRoom(20, 20)
        await controller.deleteRooms(20, 20)
        let getRoom = await controller.getRoomByID(deleteRoom._id)
        let getRoom2 = await controller.getRoomByID(deleteRoom2._id)
        let getRoom3 = await controller.getRoomByID(deleteRoom3._id)
        assert.equal(null, getRoom)
        assert.equal(null, getRoom2)
        assert.equal(null, getRoom3)
    })

    after(async function () {
        await controller.deleteRooms(18, 18)
        await controller.deleteRooms(22, 22)


        await personController.deletePerson(person._id)

    })
});