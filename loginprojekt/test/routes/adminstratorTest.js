const assert = require("chai").assert;
const adminstrator = require("../../routes/administrator");
const request = require("request");
const baseURL = "http://localhost:8000/administrator";

describe("GET", function() {
  it("/administrator should return status code 200", function() {
    request.get(baseURL, function(error, response, body) {
      assert.equal(200, response.statusCode);
    });
  });
});
