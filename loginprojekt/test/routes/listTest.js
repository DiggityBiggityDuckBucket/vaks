const assert = require("chai").assert,
  chai = require("chai"),
  chaiHttp = require("chai-http");
const baseURL = "http://localhost:8000/list";

chai.use(chaiHttp);

describe("GET List", function () {
  it("/list should return status code 200", function (done) {
    chai
      .request(baseURL)
      .get("/")
      .end(function (error, response) {
        assert.equal(200, response.statusCode);
        done();
      });
  });
});
