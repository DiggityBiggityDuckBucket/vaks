const assert = require("chai").assert,
  chai = require("chai"),
  chaiHttp = require("chai-http");
const baseURL = "http://localhost:8000";

chai.use(chaiHttp);
let expect = chai.expect;

describe("GET ", function () {
  it("/ should return status code 200", function (done) {
    chai
      .request(baseURL)
      .get("/")
      .end(function (error, response) {
        assert.equal(200, response.statusCode);
        done();
      });
  });
  it("/calendar should return status code 200", function (done) {
    chai
      .request(baseURL)
      .get("/calendar")
      .end(function (error, response) {
        assert.equal(200, response.statusCode);
        done();
      });
  });
});

describe("post", function () {
  it("post to /login should return status code 200", function (done) {
    chai
      .request(baseURL)
      .post("/login")
      .type("form")
      .send({
        _method: "post",
        navn: "0101",
        password: "123"
      })
      .end(function (error, response) {
        assert.equal(error, null);
        assert.equal(response.statusCode, 200);
        done();
      });
  });
  it("Wrong post to /login should return status code 403 (Forbidden)", function (done) {
    chai
      .request(baseURL)
      .post("/login")
      .type("form")
      .send({
        _method: "post",
        navn: "0101",
        password: "789"
      })
      .end(function (error, response) {
        assert.equal(error, null);
        assert.equal(response.statusCode, 403);
        done();
      });
  });
});
