const assert = require("chai").assert,
  chai = require("chai"),
  chaiHttp = require("chai-http");
const baseURL = "localhost:8000/change/1+1";
const baseURL2 = "localhost:8000/change"
const roomController = require("../../controllers/roomController");

chai.use(chaiHttp);
let expect = chai.expect;

let room;


describe("GET ", function () {
  it("/change/1+1 should return status code 200", function (done) {
    chai
      .request(baseURL)
      .get("/")
      .end(function (error, response) {
        assert.equal(200, response.statusCode);
        done();
      });
  });
});

describe("post", function () {
  before(async function () {
    room = await roomController.getRoom(01, 01)

  })
  it("post to /change should return status code 200 and change person", function (done) {
    chai
      .request(baseURL2)
      .post("/")
      .type("form")
      .send({
        _method: "post",
        name: "Sune",
        password: "123",
        phonenumber: "97885675",
        roomID: "" + room._id
      })
      .end(function (error, response) {
        assert.equal(error, null);
        assert.equal(response.statusCode, 200);
        done();
      });
  });
  it("check if rooms person is updated", async function () {
    let room = await roomController.getRoom(1, 1);
    assert.equal(room.person.name, "Sune")
    assert.equal(room.person.password, "123")
    assert.equal(room.person.phone, "97885675")
  });
});
