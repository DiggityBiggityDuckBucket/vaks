const mongoose = require('mongoose')
const Schema = mongoose.Schema

const cleaningRecordSchema = new mongoose.Schema({
    completed: Boolean,
    note: String,
    week: { type: Schema.Types.ObjectId, ref: 'Week' },
    cleaningItem: { type: Schema.Types.ObjectId, ref: 'CleaningItem', default: undefined },
    room: { type: Schema.Types.ObjectId, ref: 'Room', default: undefined }
});

const cleaningRecord = mongoose.model('CleaningRecord', cleaningRecordSchema)
module.exports = cleaningRecord
