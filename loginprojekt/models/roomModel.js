const mongoose = require('mongoose')
const Schema = mongoose.Schema

const værelseSchema = new mongoose.Schema({
    roomNumber: Number,
    hallNumber: Number,
    person: {type: Schema.Types.ObjectId, ref: 'Person', default: undefined}
});

const room = mongoose.model('Room', værelseSchema)
module.exports = room

