const mongoose = require('mongoose')

const weekSchema = new mongoose.Schema({
    weekNumber: { type: Number, min: 1, max: 52 },
    year: { type: Number, min: 1970, max: 3000 }
});

const week = mongoose.model('Week', weekSchema)
module.exports = week
