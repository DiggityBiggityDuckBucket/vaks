const mongoose = require('mongoose')

const cleaningItemSchema = new mongoose.Schema({
    name: String,
    description: String,
});

const cleaningItem = mongoose.model('CleaningItem', cleaningItemSchema)
module.exports = cleaningItem
