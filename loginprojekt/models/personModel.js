const mongoose = require('mongoose')

const personSchema = new mongoose.Schema({
    name: String,
    phone: String,
    password: String ,
});

const person = mongoose.model('Person', personSchema)
module.exports = person

