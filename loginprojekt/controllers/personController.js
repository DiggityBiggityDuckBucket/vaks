let Person = require('../models/personModel.js')
require("../app.js");
const mongoose = require('mongoose')


exports.createPerson = async function (name, phone) {
    const person = new Person({
        name: name,
        phone: phone,
        password: '123'
    });
    return await person.save();
}

exports.getPerson = async function (id) {
    return await Person.findById({
        _id: id
    })
}

exports.getPersonByPhone = async function (phone) {
    return await Person.findOne({
        phone: phone
    })
}

exports.deletePerson = async function (personID) {
    await Person.deleteOne({
        _id: personID
    }).exec()
}

exports.setPassword = async function (personID, newPass) {
    let person = await Person.findById(personID)
    person.password = newPass;
    await person.save();
}