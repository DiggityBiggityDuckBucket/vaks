let Week = require('../models/weekModel')
require("../app.js");

exports.createWeek = function (weekNumber, year) {
    const week = new Week({
        weekNumber: weekNumber,
        year: year,
    });
    return week.save();
}

exports.getWeeks = function () {
    return Week.find().exec()
}

exports.getWeek = async function (weekNumber, year) {
    return await Week.findOne({ weekNumber: weekNumber, year: year }).exec()
}

exports.deleteWeek = async function (weekNumber, year) {
    await Week.deleteOne({ weekNumber: weekNumber, year: year }).exec()
}
