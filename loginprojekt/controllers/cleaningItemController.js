let CleaningItem = require("../models/cleaningItemModel");
require("../app.js");

exports.createcleaningItem = function (name, description) {
  const cleaningItem = new CleaningItem({
    name: name,
    description: description
  });
  return cleaningItem.save();
};

exports.getCleaningItems = function () {
  return CleaningItem.find().exec();
};


exports.getCleaningItem = function (name) {
  return CleaningItem.findOne({
    name: name
  }).exec();
};

exports.deleteCleaningItem = async function (name) {
  await CleaningItem.deleteOne({
    name: name
  }).exec();
};