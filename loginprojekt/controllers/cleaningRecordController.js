let CleaningRecord = require('../models/cleaningRecordModel')
let roomController = require("../controllers/roomController");
const Schema = require('mongoose').Schema
require("../app.js");

exports.createCleaningRecord = function (weekID, cleaningItemID, note = "") {
    const cleaningRecord = new CleaningRecord({
        completed: false,
        note: note,
        week: weekID,
        cleaningItem: cleaningItemID,
        room: undefined,
    });
    return cleaningRecord.save();
}

exports.getCleaningRecords = function () {
    return CleaningRecord.find().populate('week').populate('cleaningItem').populate('room').exec()
}

exports.getCleaningRecordsForWeek = function (weekID) {
    return CleaningRecord.find({ week: weekID }).populate('week').populate('cleaningItem').populate('room').exec()
}

exports.setCompleted = function (completed) {
    completed = completed;
}

exports.setNote = function (newNote) {
    note = newNote;
}

exports.assignRoomToRecords = async function (roomID, weekID) {
    let records = await CleaningRecord.find({ week: weekID }).populate('week').populate('cleaningItem').exec()
    for (let record of records){
        record.room = roomID;
        await record.save();

    }
};

exports.deleteCleaningRecord = function(weekID) {
    CleaningRecord.deleteOne({week: weekID}).exec()

}

