let Room = require("../models/roomModel");
let personController = require("../controllers/personController");
require("../app.js");

exports.createRoom = function (hallNumber, roomNumber) {
  const room = new Room({
    roomNumber: roomNumber,
    hallNumber: hallNumber,
  });
  return room.save();
};

exports.getRooms = async function () {
  return await Room.find().populate('person').exec();
};

exports.getHalls = async function () {
  const halls = []
  const rooms = await Room.find().exec()
  await rooms.forEach(room => {
    if (!halls.includes(room.hallNumber))
      halls.push(room.hallNumber)
  });
  return halls
}

exports.getRoomsByHall = async function (hall) {
  let rooms = await Room.find({ 'hallNumber': hall }).populate('person').exec()
  rooms.sort(function (a, b) {
    return a.roomNumber - b.roomNumber;
  });
  return rooms;
}


exports.getRoom = async function (hallNumber, roomNumber) {
  return await Room.findOne({
    roomNumber: roomNumber,
    hallNumber: hallNumber
  }).populate('person').exec();
};

exports.getRoomByID = async function (roomID) {
  return await Room.findById(roomID).populate('person').exec();
};

exports.addPersonToRoom = async function (roomID, personID) {
  const room = await Room.findById(roomID);
  if (room.person != undefined) {
    await personController.deletePerson(room.person._id)
  }
  room.person = personID
  await room.save();
}

exports.deleteRoom = async function (roomNumber, hallNumber) {
  await Room.deleteOne({
    hallNumber: hallNumber,
    roomNumber: roomNumber
  }).exec()
}

exports.deleteRooms = async function (roomNumber, hallNumber) {
  await Room.deleteMany({
    hallNumber: hallNumber,
    roomNumber: roomNumber
  }).exec()
}