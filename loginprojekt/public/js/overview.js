function sortTable() {
    var table, rows, switching, i, x, y, a, b, shouldSwitch;
    table = document.getElementById("table");
    switching = true;
    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("TR");
        for (i = 0; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[1];
            y = rows[i + 1].getElementsByTagName("TD")[1];
            //check if the two rows should switch place:
            if (Number(x.innerHTML) < Number(y.innerHTML)) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }


        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switchcand mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
  
    for (i = 0; i < (rows.length - 1); i++) {
        /*Get the two elements you want to compare,one from current row and one from the next:*/
        for (j = i + 1; j < (rows.length); j++) {
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[j].getElementsByTagName("TD")[0];
            a = rows[i].getElementsByTagName("TD")[1];
            b = rows[j].getElementsByTagName("TD")[1];
            //check if the two rows should switch place:
            if (Number(x.innerHTML) == Number(y.innerHTML)) {
                //if so, swap
                if (Number(a.innerHTML) > Number(b.innerHTML)) {
                    rows[i].parentNode.insertBefore(rows[j], rows[i]);

                }
            }
        }
    }
}

sortTable()