// const cleaningRecordController = require('../../controllers/cleaningRecordController')
// const weekController = require('../../controllers/weekController')

Date.prototype.getWeek = function (dowOffset) {
    /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.meanfreepath.com */
    dowOffset = typeof (dowOffset) == 'int' ? dowOffset : 0; //default dowOffset to zero
    var newYear = new Date(this.getFullYear(), 0, 1);
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() -
        (this.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) / 86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if (day < 4) {
        weeknum = Math.floor((daynum + day - 1) / 7) + 1;
        if (weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1, 0, 1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    } else {
        weeknum = Math.floor((daynum + day - 1) / 7);
    }
    return weeknum;
};

var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();
var week = today.getWeek();

today = dd + '/' + mm + '/' + yyyy;
document.write("Dags dato: " + today);
document.write("<br>");
document.write("Uge: " + week);



var listElementsArray = document.getElementsByTagName('li')

for (var i = 0; i < listElementsArray.length; i++) {
    listElementsArray[i].addEventListener('click', function (e) {
            doStuff(e);
        }

    );
}

function doStuff(e) {
    let p = document.getElementById("selectedWeek")
    p.textContent = "Valgt uge: " + e.target.id
}