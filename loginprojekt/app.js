//INITIALISERING
const express = require("express");
const app = express();
const config = require("./config");
var session = require('express-session');
var path = require("path");
var cookieParser = require('cookie-parser');
app.set("view engine", "pug");

// inkluder schemas
var fs = require('fs');
fs.readdirSync(__dirname + '/models').forEach(function (file) {
  if (~file.indexOf('.js')) require(__dirname + '/models/' + file);
});
app.use(express.static(path.join(__dirname, "public")));

//MONGODB opsætning
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect(config.mongoDBHost, {
  useNewUrlParser: true,
  useCreateIndex: true,
  autoIndex: false,
  useUnifiedTopology: true
});


// initialize cookie-parser to allow us access the cookies stored in the browser. 
app.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
  key: 'user_sid',
  secret: 'baconpancakes',
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 600000
  }
}));


// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
    res.clearCookie('user_sid');
  }
  next();
});


// middleware function to check for logged-in users
let sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_sid) {
    next();
  } else {

    res.redirect('/');
  }
};

// Routes
const loginRouter = require("./routes/login");
const overviewRouter = require("./routes/overview");
const administratorRouter = require("./routes/administrator");
const listRouter = require("./routes/list");
const calendarRouter = require("./routes/calendar");
const changeRouter = require("./routes/change");
const cleaningItemsRouter = require("./routes/cleaningItems");


app.use("/", loginRouter);
app.use("/overview", sessionChecker, overviewRouter);
app.use("/administrator", sessionChecker, administratorRouter);
app.use("/list", sessionChecker, listRouter);
app.use("/calendar", sessionChecker, calendarRouter);
app.use("/change", sessionChecker, changeRouter);
app.use("/cleaningItems", sessionChecker, cleaningItemsRouter);


app.listen(config.PORT, () => console.log("lytter på port " + config.PORT));

module.exports = mongoose;