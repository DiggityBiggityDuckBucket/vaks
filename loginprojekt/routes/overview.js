const express = require('express')
const router = express.Router()
const værelseController = require('../controllers/roomController')

//pæn bodyparser
router.use(express.json())
router.use(express.urlencoded({
    extended: true
}))


router.get('/', (req, res) => {
    værelseController.getRooms().then(data => {
        res.render("overview", {
            værelser: data,
            person: req.session.user
        });

    })
})

module.exports = router