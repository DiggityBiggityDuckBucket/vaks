const express = require("express");
const router = express.Router();
const roomController = require("../controllers/roomController");

//pæn bodyparser
router.use(express.json());
router.use(
  express.urlencoded({
    extended: true
  })
);

router.get("/", (req, res) => {
  roomController.getRoom(1,1).then(data => {
    res.render("login", {
      værelse: data
    });
  });
});

router.post("/login", (req, res) => {

  let regex = /\d\d/gm
  let str = req.body.navn

  let hallNumber = regex.exec(str)
  let roomNumber = regex.exec(str)
  let password = req.body.password

  // START PÅ LOGIN STUFF
  roomController.getRoom(hallNumber,roomNumber).then( 
    function (room) {
      if (!room) {
        res.status(403).render("login", {
          msg: "No such room"
        });
      } else if (!room.person) {
        res.status(403).render("login", {
          msg: "No person in that room"
        });
      } else if (room.person.password != password) {
        res.status(403).render("login", {
          msg: "Wrong password"
        });
      } else {
        req.session.user = room.person
        roomController.getRooms().then(data => {
          res.render("overview", {
            værelser: data,
            person: room.person
          });
        });
      }
    })
    .catch(function () {
      res.status(403).render("login", {
        msg: "invalid input"
      });
      res.sendStatus(403);
    })
});


module.exports = router;