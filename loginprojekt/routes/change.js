const express = require("express");
const router = express.Router();

const roomController = require("../controllers/roomController");
const personController = require("../controllers/personController");

//pæn bodyparser
router.use(express.json());
router.use(
  express.urlencoded({
    extended: true
  })
);

router.get("/:id", (req, res) => {
  const roomtag = req.params.id.split('+')
  const hallNumber = roomtag[0]
  const roomNumber = roomtag[1]

  roomController.getRoom(hallNumber, roomNumber).then(
    async function (room) {
      res.render("change", {
        room: room
      });
    }
  )

});

router.post("/", (req, res) => {
  let name = req.body.name;
  let phoneNumber = req.body.phonenumber;
  let password = req.body.password;
  let roomID = req.body.roomID;
  
  roomController.getRoomByID(roomID).then(
  async function(room) {
    let person = await personController.createPerson(name, phoneNumber)
     personController.setPassword(person._id, password)
     roomController.addPersonToRoom(room._id, person)
    res.redirect('../administrator/' + room.hallNumber) 
  })
})

module.exports = router;

