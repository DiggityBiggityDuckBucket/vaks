const express = require("express");
const router = express.Router();
const cleaningItemController = require('../controllers/cleaningItemController')

//pæn bodyparser
router.use(express.json());
router.use(
  express.urlencoded({
    extended: true
  })
);

router.get("/", (req, res) => {
 cleaningItemController.getCleaningItems().then(data => {
   res.render('list', {
     items: data
   })
 })

});

module.exports = router;
