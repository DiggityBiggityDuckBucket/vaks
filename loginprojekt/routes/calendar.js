const cleaningRecordController = require("../controllers/cleaningRecordController");
const weekController = require("../controllers/weekController");

const express = require("express");
const router = express.Router();

//pæn bodyparser
router.use(express.json());
router.use(
  express.urlencoded({
    extended: true
  })
);

router.get("/", async (req, res) => {

  res.redirect('calendar/1')
});

router.get("/:week", async (req, res) => {
  let week = await weekController.getWeek(req.params.week,2019);
  let data = await cleaningRecordController.getCleaningRecordsForWeek(week._id)
  res.render("calendar", {cleaningrecords: data});
});

module.exports = router;
