const express = require("express");
const router = express.Router();
const værelseController = require("../controllers/roomController");

router.get("/", (req,res)=> {
  res.redirect("/administrator/1")
})

router.get("/:id", async (req, res) => {
  let hall = req.params.id
  const halls = await værelseController.getHalls()
  const rooms = await værelseController.getRoomsByHall(hall)
  res.render("administrator",{rooms: rooms, halls: halls})
})        



module.exports = router;
