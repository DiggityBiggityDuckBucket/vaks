const roomController = require('./controllers/roomController')
const personController = require('./controllers/personController')
const cleaningItemController = require('./controllers/cleaningItemController')
const cleaningRecordController = require('./controllers/cleaningRecordController')
const weekController = require('./controllers/weekController')


const config = require('./config.js')
const mongoose = require('mongoose');



mongoose.connect(config.mongoDBHost, {
        useNewUrlParser: true,
        useCreateIndex: true,
        autoIndex: false,
        useUnifiedTopology: true
    }, function () {

        // mongoose.connection.db.dropCollection('rooms', function (err, result) {
        //     if (err) {
        //         console.log(err)
        //     } else {
        //         console.log(result);
        //     }
        // })

        // mongoose.connection.db.dropCollection('people', function (err, result) {
        //     if (err) {
        //         console.log(err)
        //     } else {
        //         console.log(result);
        //     }
        // })

        // mongoose.connection.db.dropCollection('weeks', function (err, result) {
        //     if (err) {
        //         console.log(err)
        //     } else {
        //         console.log(result);
        //     }
        // })

        // mongoose.connection.db.dropCollection('cleaningitems', function (err, result) {
        //     if (err) {
        //         console.log(err)
        //     } else {
        //         console.log(result);
        //     }
        // })

        // mongoose.connection.db.dropCollection('cleaningrecords', function (err, result) {
        //     if (err) {
        //         console.log(err)
        //     } else {
        //         console.log(result);
        //     }
        // })

    })
    .then(async function () {
        // await createRooms();
        // await createPeople();
        // await createWeeks(2019);
        // await createCleaningItems();
    })
    .then(async function () {

        // addPeopleToRooms();
        // addCleaningItemsToRecords();
        // addRoomsToCleaningRecords();
    })


async function createRooms() {
    await roomController.createRoom(01, 01).then(res => {
        console.log(res);
    })

    await roomController.createRoom(01, 02).then(res => {
        console.log(res);
    })

    await roomController.createRoom(01, 03).then(res => {
        console.log(res);
    })

    await roomController.createRoom(01, 04).then(res => {
        console.log(res);
    })

    await roomController.createRoom(02, 01).then(res => {
        console.log(res);
    })

    await roomController.createRoom(02, 02).then(res => {
        console.log(res);
    })

    await roomController.createRoom(02, 03).then(res => {
        console.log(res);
    })

    await roomController.createRoom(02, 04).then(res => {
        console.log(res);
    })
}

function createPeople() {
    personController.createPerson("ib", "12345678").then(res => {
        console.log(res);
    })

    personController.createPerson("Troels", "66699988").then(res => {
        console.log(res);
    })

    personController.createPerson("Suneboi", "52461835").then(res => {
        console.log(res);
    })

    personController.createPerson("Nerdygirl", "66666666").then(res => {
        console.log(res);
    })

    personController.createPerson("Flemse", "74123658").then(res => {
        console.log(res);
    })

    personController.createPerson("Hestepigen", "74859621").then(res => {
        console.log(res);
    })

    personController.createPerson("Skaterboi", "85416325").then(res => {
        console.log(res);
    })

}

function createCleaningItems() {

    cleaningItemController.createcleaningItem("Ovn", "komfuret skal aftørres og riste skal afristes og Troels er helt ristet")
    cleaningItemController.createcleaningItem("Håndvask", "Skal være vasket og aftørret")
    cleaningItemController.createcleaningItem("Køleskab", "Skal være vasket og aftørret")
    cleaningItemController.createcleaningItem("Overflader", "Skal være vasket og aftørret")
    cleaningItemController.createcleaningItem("Gulv", "Skal være vasket og aftørret")


}

function createWeeks(year) {

    for (let i = 1; i < 53; i++) {
        weekController.createWeek(i, year)
    }

}

async function addPeopleToRooms() {
    vær0101 = await roomController.getRoom(01, 01)
    vær0102 = await roomController.getRoom(01, 02)
    vær0103 = await roomController.getRoom(01, 03)
    vær0104 = await roomController.getRoom(01, 04)
    vær0201 = await roomController.getRoom(02, 01)
    vær0202 = await roomController.getRoom(02, 02)
    vær0203 = await roomController.getRoom(02, 03)
    vær0204 = await roomController.getRoom(02, 04)

    Troels = await personController.getPersonByPhone("66699988")
    Sune = await personController.getPersonByPhone("52461835")
    Emilie = await personController.getPersonByPhone("66666666")
    Ib = await personController.getPersonByPhone("12345678")
    Flemse = await personController.getPersonByPhone("74123658")
    Hestepigen = await personController.getPersonByPhone("74859621")
    Skaterboi = await personController.getPersonByPhone("85416325")

    roomController.addPersonToRoom(vær0101._id, Troels._id)
    roomController.addPersonToRoom(vær0102._id, Sune._id)
    roomController.addPersonToRoom(vær0103._id, Ib._id)
    roomController.addPersonToRoom(vær0104._id, Flemse._id)
    roomController.addPersonToRoom(vær0201._id, Emilie._id)
    roomController.addPersonToRoom(vær0203._id, Skaterboi._id)
    roomController.addPersonToRoom(vær0204._id, Hestepigen._id)
}

async function addCleaningItemsToRecords() {

    let cleaningitems = await cleaningItemController.getCleaningItems();

    for (let w = 1; w < 53; w++) {

        let week = await weekController.getWeek(w, 2019);


        for (let i = 0; i < cleaningitems.length; i++) {
            cleaningRecordController.createCleaningRecord(week._id, cleaningitems[i]._id)
        }

    }
}


async function addRoomsToCleaningRecords() {
    let rooms = await roomController.getRoomsByHall(1);
    for (let w = 1; w < 53; w++) {
        let week = await weekController.getWeek(w, 2019);
        cleaningRecordController.assignRoomToRecords(rooms[w % rooms.length]._id, week._id)
    }
}